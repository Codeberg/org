## Codeberg e.V.

Codeberg is a non-profit organisation dedicated to build and maintain supporting infrastructure for the creation, collection, dissemination, and archiving of Free and Open Source Software. If you have questions, suggestions or comments **regarding Codeberg as a platform**, please do not hesitate to contact us at [contact@codeberg.org](mailto:contact@codeberg.org).
Please note that all **actual content** on Codeberg is managed by **user accounts or project teams**.
Unless the content violates our Terms of Use, please **contact them directly.**

**If you encounter abusive or malicious user content, please notify us at  [abuse@codeberg.org](mailto:abuse@codeberg.org).**

**If you encounter security issues in the software we use**, please send them to the Forgejo security team via [security@forgejo.org].
They are collaborating with Codeberg and it reduces the time to fix. [(Details and PGP key)](https://forgejo.org/.well-known/security.txt)
For other issues, please contact us at the address provided in the imprint below.


## Impressum nach §5 DDG (Imprint according to German Law)

An Impressum (Latin _impressum_, usually translated to _"Imprint"_ in analogy to the printer's imprint according to UK law) is the legally mandated statement of the ownership and authorship of a document, which must be included in books, newspapers, magazines and websites published in Germany and certain other German-speaking countries, such as Austria and Switzerland. The Digitale-Dienste-Gesetz (DDG, _"Digital Services Act"_) mandates an Impressum, ours you find here:

> Codeberg e.V.  
> Arminiusstraße 2 - 4  
> 10551 Berlin
> 
> E-Mail: contact@codeberg.org  
> Direct communication: https://matrix.to/#/#codeberg.org:matrix.org
> 
> Geschäftsführender Vorstand: Otto Richter
>
> Eingetragen im Vereinsregister des Amtsgerichts Charlottenburg VR36929.


## Gemeinnützigkeit (recognition of status as non-profit NGO, recognition as tax-excempt entity)

Codeberg e.V. is recognized by German tax authorities as tax-exempt non-profit organization for the common good. In classic German legalese this reads: 

```text
Der Codeberg e.V. ist mit Bescheid vom 23. Juni 2020 vom Finanzamt für Körperschaften I (Berlin) steuerbegünstigt
und erfüllt die Voraussetzungen nach §51, §59, §60 und §61 AO.

Der Codeberg e.V. fördert folgende gemeinnützige Zwecke:

- Förderung von Wissenschaft und Forschung (§52 Abs. 2 Satz 1 Nr. (n) 1 AO),
- Förderung der Volks- und Berufsbildung einschließlich der Studentenhilfe (§52 Abs. 2 Satz 1 Nr. (n) 7 AO).

Damit ist der Codeberg e.V. zur Ausstellung von Zuwendungsbestätigungen für Spenden und Mitgliedsbeiträgen nach
§50 Abs. 1 EStDV berechtigt.
```


## SEPA IBAN for donations

```text
IBAN DE90 8306 5408 0004 1042 42
BIC  GENODEF1SLR
```


## SSH Fingerprints

Below you find the fingerprints for SSH hostkey verification:

```text
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBL2pDxWr18SoiDJCGZ5LmxPygTlPu+cCKSkpqkvCyQzl5xmIMeKNdfdBpfbCGDPoZQghePzFZkKJNR/v9Win3Sc=
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8hZi7K1/2E2uBX8gwPRJAHvRAob+3Sn+y2hxiEhN0buv1igjYFTgFO2qQD8vLfU/HT/P/rqvEeTvaDfY1y/vcvQ8+YuUYyTwE2UaVU5aJv89y6PEZBYycaJCPdGIfZlLMmjilh/Sk8IWSEK6dQr+g686lu5cSWrFW60ixWpHpEVB26eRWin3lKYWSQGMwwKv4LwmW3ouqqs4Z4vsqRFqXJ/eCi3yhpT+nOjljXvZKiYTpYajqUC48IHAxTWugrKe1vXWOPxVXXMQEPsaIRc2hpK+v1LmfB7GnEGvF1UAKnEZbUuiD9PBEeD5a1MZQIzcoPWCrTxipEpuXQ5Tni4mN
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIVIC02vnjFyL+I4RHfvIGNtOgJMe769VTF1VR4EB3ZB
```

## API

Please refer to the [official Forgejo Swagger API documentation](https://forgejo.org/docs/latest/user/api-usage/) for information to access the API endpoints.
