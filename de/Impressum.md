Bitte beachte: Dies ist eine inoffizielle Übersetzung des [englischen Originaldokuments](../Imprint.md). Bitte lies auch die weiteren Hinweise im Überordner dieses Repositorys.

## Codeberg e.V.

Codeberg ist eine gemeinnützige Organisation, die sich der Entwicklung und dem Betrieb einer Infrastruktur für die Entwicklung, Sammlung und Archivierung Freier und Open-Source-Software widmet. Falls Du Fragen, Vorschläge oder Kommentare hast, zögere nicht, uns über [contact@codeberg.org](mailto:contact@codeberg.org) zu kontaktieren.

## Impressum nach §5 TMG 

Das Impressum ist die rechtliche Angabe über die Urheber- bzw. Autorschaft bezüglich eines Dokuments, welche in Deutschland und in anderen deutschsprachigen Ländern wie etwa der Schweiz und Österreich in veröffentlichten Büchern, Zeitungen, Websites und anderen Print- oder Telemedien enthalten sein muss. Das Telemediengesetz (TMG) erfordert ein solches Impressum; unseres ist folgendes:

```text
Codeberg e.V.
Arminiusstraße 2 - 4
10551 Berlin

E-Mail: contact@codeberg.org

Geschäftsführender Vorstand: Otto Richter
Eingetragen im Vereinsregister des Amtsgerichts Charlottenburg VR36929.
```

## Gemeinnützigkeit 

Codeberg e.V. ist von deutschen Steuerbehörden als steuerbefreite gemeinnützige Organisation anerkannt. In Beamtendeutsch liest sich das so:

```text
Der Codeberg e.V. ist mit Bescheid vom 23. Juni 2020 vom Finanzamt für Körperschaften I (Berlin) steuerbegünstigt
und erfüllt die Voraussetzungen nach §51, §59, §60 und §61 AO.

Der Codeberg e.V. fördert folgende gemeinnützige Zwecke:

- Förderung von Wissenschaft und Forschung (§52 Abs. 2 Satz 1 Nr. (n) 1 AO),
- Förderung der Volks- und Berufsbildung einschließlich der Studentenhilfe (§52 Abs. 2 Satz 1 Nr. (n) 7 AO).

Damit ist der Codeberg e.V. zur Ausstellung von Zuwendungsbestätigungen für Spenden und Mitgliedsbeiträgen nach
§50 Abs. 1 EStDV berechtigt.
```


## SEPA IBAN für Spenden

```text
IBAN DE90 8306 5408 0004 1042 42
BIC  GENODEF1SLR
```


## SSH Fingerprints

Hier findest du die Fingerabdrücke für die SSH-Host-Key-Überprüfung:

```text
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBL2pDxWr18SoiDJCGZ5LmxPygTlPu+cCKSkpqkvCyQzl5xmIMeKNdfdBpfbCGDPoZQghePzFZkKJNR/v9Win3Sc=
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC8hZi7K1/2E2uBX8gwPRJAHvRAob+3Sn+y2hxiEhN0buv1igjYFTgFO2qQD8vLfU/HT/P/rqvEeTvaDfY1y/vcvQ8+YuUYyTwE2UaVU5aJv89y6PEZBYycaJCPdGIfZlLMmjilh/Sk8IWSEK6dQr+g686lu5cSWrFW60ixWpHpEVB26eRWin3lKYWSQGMwwKv4LwmW3ouqqs4Z4vsqRFqXJ/eCi3yhpT+nOjljXvZKiYTpYajqUC48IHAxTWugrKe1vXWOPxVXXMQEPsaIRc2hpK+v1LmfB7GnEGvF1UAKnEZbUuiD9PBEeD5a1MZQIzcoPWCrTxipEpuXQ5Tni4mN
# codeberg.org:22 SSH-2.0-OpenSSH_7.9p1 Debian-10+deb10u1
codeberg.org ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIVIC02vnjFyL+I4RHfvIGNtOgJMe769VTF1VR4EB3ZB
```

## API

Bitte schaue in die [offizielle Gitea-Swagger-API-Dokumentation (englisch)](https://docs.gitea.io/en-us/api-usage/) für weitere Informationen zum API-Zugang.
